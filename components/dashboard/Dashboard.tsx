'use client'
import React, { FC, useEffect, useState } from 'react'
import { Button } from '../ui/button';

interface DashboardProps {
  
}

const Dashboard: FC<DashboardProps> = ({  }) => {
    const [time, setTime] = useState<number>(0);

    const targetDate = 60 * 60 * 24 * 2;

    useEffect(() => {
        const startTime = localStorage.getItem('Start Time');
        if (!startTime) {
            return;
        }
        if (time === targetDate ) {
            return;
        }
        const interval = setInterval(() => {
            setTime(Date.now() - Number(startTime));
        }, 1000);
        
        return () => clearInterval(interval);
    }, [targetDate, time])


    const handleStart = () => {
        localStorage.setItem('Start Time', Date.now().toString());
    }

    const percent = (((time / 1000) / targetDate) * 100).toFixed(2);
    
  return (
    <div className='h-screen w-full flex justify-center flex-col gap-6 items-center bg-slate-800'>
        <h3 className='text-2xl text-slate-50 font-semibold' >Quit game time</h3>
        <p className='text-slate-50 font-semibold'>Time : {Math.floor(time / 1000)} seconds</p>
        <p className='text-slate-50 font-semibold'>Target Remaining : {Math.floor(targetDate - time / 1000)} seconds ({percent}%)</p>
        <div className='w-64 bg-white h-1  rounded-lg relative'>
            <div style={{ width: `${(percent)}%` }} className='top-0 left-0 bg-green-500 h-1 rounded-lg absolute'></div>

        </div>
            <p>Test haha</p>
    
        <Button onClick={handleStart} className='flex' variant={'outline'}>{time > 0 ? 'Restart' : "Start"}</Button>
    </div>
  )
}

export default Dashboard;