import Dashboard from "@/components/dashboard/Dashboard";
import { Button } from "@/components/ui/button";
import { cn } from "@/lib/utils";
import { Metadata } from "next";
import Image from "next/image";

export const metadata  : Metadata = {
  title: "Home Page",
  description: "This is home page"
}

export default function Home() {
  return (
    <main >
      <Dashboard/>
    </main>
  );
}
